package com.njk.fpinscala

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class FactorialSpec extends FlatSpec {
  "Factorial" should "give 1 for 0"  in {
    assert(MyModule.factorial(0) === 1)
  }
  it should "give 1 for 1"  in {
    assert(MyModule.factorial(1) === 1)
  }
  it should "give 2 for 2" in {
    assert(MyModule.factorial(2) === 2)
  }
  it should "give 6 for 3" in {
    assert(MyModule.factorial(3) === 6)
  }
  it should "give 24 for 4" in {
    assert(MyModule.factorial(4) == 24)
  }
  it should "give 3628800 for 10" in {
    assert(MyModule.factorial(10) == 3628800)
  }
}

@RunWith(classOf[JUnitRunner])
class FibSpec extends FlatSpec {
  "Nth Fibonacci solver" should "give 0 for 0" in {
    assert(MyModule.fib(0) === 0)
  }
  it should "give 1 for 1" in {
    assert(MyModule.fib(1) === 1)
  }
  it should "give 1 for 2" in {
    assert(MyModule.fib(2) === 1)
  }
  it should "give 2 for 3" in {
    assert(MyModule.fib(3) === 2)
  }
  it should "give 3 for 4" in {
    assert(MyModule.fib(4) === 3)
  }
  it should "give 5 for 5" in {
    assert(MyModule.fib(5) === 5)
  }
  it should "give 8 for 6" in {
    assert(MyModule.fib(6) === 8)
  }
  it should "give 34 for 9" in {
    assert(MyModule.fib(9) === 34)
  }
}

@RunWith(classOf[JUnitRunner])
class IsSortedSpec extends FlatSpec {
  "isSorted with lessThan integer comparator" should "give false for [1,5,3,4]" in {
    assert(MyModule.isSorted(Array(1,5,3,4), (p: Int, c: Int) => p < c) === false)
  } 
  it should "give true for [1,2,3,4]" in {
    assert(MyModule.isSorted(Array(1,2,3,4), (p: Int, c:Int) =>  p < c) === true)
  } 
  it should "give true for empty array"  in {
    assert(MyModule.isSorted(Array(), (p:Int, c:Int) => p < c) === true)
  }
  it should "give true for array with 1 element"  in {
    assert(MyModule.isSorted(Array(10), (p:Int, c:Int) => p < c) === true)
  }
}