package com.njk.fpinscala

import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.FunSuite

@RunWith(classOf[JUnitRunner])
class ListOperationTestSuite extends FunSuite {
  test("append operation when first list is empty") {
    assert(List.append(Nil, List(1,2,3)) === List(1,2,3))
    assert(List.append(Nil, Nil) === Nil)
  }
  
  test("append operation when second list is empty") {
    assert(List.append(List("Naresh", "Khalasi"), Nil) === List("Naresh", "Khalasi"))
  }
  
  test("append operation when both lists are non-empty") {
    assert(List.append(List("naresh", "jasmine"), List("krishang")) === List("naresh", "jasmine", "krishang"))
  }
  
  test("append operation when both lists contain different data types") {
    assert(List.append(List("naresh", "khalasi"), List(1,3)) === List("naresh", "khalasi", 1, 3))
  }
  
  test("tail operation when the list contains one element") {
    assert(List.tail(List(1)) === Nil)
  }
  
  test("tail operation when the list contains two elements") {
    assert(List.tail(List(1,2)) === List(2))
  }
  
  test("tail operation when the list contains more than 2 elements") {
    assert(List.tail(List(1,2,3,4)) === List(2,3,4))
  }
  
  test("drop operation when the list is empty") {
    assert(List.drop(Nil, 2) === Nil)
  }
  
  test("drop operation when the list has one element") {
    assert(List.drop(List(1), 1) === Nil)
  }
  
  test("drop operation when the list has 2 elements") {
    assert(List.drop(List(1,3), 1) === List(3))
  }
  
  test("drop operation when the list has more than 2 elements") {
    assert(List.drop(List(1,2,3,4,5), 2) === List(3,4,5))
  }
  
  test("dropWhile operation when the list is empty") {
    assert(List.dropWhile(Nil)(_ => false) === Nil)
  }
  
  test("dropWhile operation when the list has 1 element") {
    assert(List.dropWhile(List(1))(x => x > 0) === Nil)
  }
  
  test("dropWhile operation when the list has 2 elements") {
    assert(List.dropWhile(List(1,2))(x => x < 2) === List(2))
  }
  
  test("dropWhile operation when the list has more than 2 elements") {
    assert(List.dropWhile(List(1,2,3,4))(x => x < 3) === List(3,4))
  }
  
  test("setHead operation on a list with one element") {
    assert(List.setHead(List(1), 0) === List(0))
  }
  
  test("setHead operation on a list with more than one element") {
    assert(List.setHead(List(1,2,3), 0) === List(0,2,3))
  }
  
  test("foldRight operation with Cons operator") {
    val x = List.foldRight(List(1,2,3), Nil:List[Int])(Cons(_, _))
    assert(x === Cons(1, Cons(2, Cons(3, Nil))))
  }
  
  test("length of empty list") {
    assert(List.length(Nil) === 0)
  }
  
  test("length of non-empty list") {
    assert(List.length(List('a', 'b','c')) === 3)
  }

//  test("foldLeft operation with Cons operator") {
//    val x = List.foldLeft(List(1,2,3), Nil:List[Int])(Cons(_, _))
//    assert(x === Cons(1, Cons(2, Cons(3, Nil))))
//  }
  
  test("reverse of a non-empty list") {
    assert(List.reverse(List('a', 'b','c')) === List('c', 'b', 'a'))
  }
  
  test("reverse of a list with one element") {
    assert(List.reverse(List(1)) === List(1))
  }
  
  test("reverse of an empty list") {
    assert(List.reverse(Nil) === Nil)
  }
  
  test("foldRight using foldLeft") {
    assert(List.foldRightUsingFoldLeft(List(1,2,3,4), List[Int]())(Cons(_, _)) === List(1,2,3,4))
  }
  
  test("sum using foldRightUsingFoldLeft") {
    assert(List.foldRightUsingFoldLeft(List(1,2,3,4), 0)(_ + _) === 10)
  }
  
  test("append using foldRight") {
    assert(List.appendUsingFoldRight(List(1,2), List(3,4)) == List(1,2,3,4))
  }
  
  test("append using foldRight when first list is empty") {
    assert(List.appendUsingFoldRight(Nil, List(1,2)) == List(1,2))
  }
  
  test("append using foldRight when second list is empty") {
    assert(List.appendUsingFoldRight(List(1,2), Nil) == List(1,2))
  }
  
  test("flatten list of list") {
    assert(List.flatten(List(List(1,2,3), List(4,5,6))) === List(1,2,3,4,5,6))
  }
  
  test("flatten list of list - one list is empty") {
    assert(List.flatten(List(List(1,2,3), Nil, List(4,5,6))) === List(1,2,3,4,5,6))
  }
  
  test("flatten list of list - when all lists are empty") {
    assert(List.flatten(List(Nil, Nil, Nil)) === Nil)
  }
  
  test("increment items in an empty list") {
    assert(List.increment(Nil) === Nil)
  }
  
  test("increment items in a list") {
    assert(List.increment(List(1,2,3,4)) === List(2,3,4,5))
  }

  test("doubleToString of an empty list") {
    assert(List.doubleToString(Nil) === Nil)
  }
  
  test("doubleToString a list") {
    assert(List.doubleToString(List(1.0,2.1,3.2,4.567)) === List("1.0", "2.1", "3.2", "4.567"))
  }
  
  test("use map to increment an empty list") {
    assert(List.map(Nil:List[Int])(_+1) === Nil)
  }
  
  test("use map to increment a list") {
    assert(List.map(List(1,2,3,4))(_+1) === List(2,3,4,5))
  }
  
  test("use map to convert double to strings") {
    assert(List.map(List(1.0, 2.1, 3.2, 4.567))(_.toString) === List("1.0", "2.1", "3.2", "4.567"))
  }
  
  test("use filter to eliminate odd numbers") {
    assert(List.filter(List(1,2,3,4))(_ % 2 == 0) === List(2,4))
  }
}