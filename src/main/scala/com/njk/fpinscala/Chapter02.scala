package com.njk.fpinscala

object MyModule {
  def factorial(n: Int): Int = {
    @annotation.tailrec
    def go(m: Int, acc: Int): Int = 
      if (m <= 0) acc
      else go(m - 1, m * acc)
    
    go(n, 1)
  }
  
  def fib(n: Int): Int = {
    @annotation.tailrec
    def loop(m: Int, prev: Int, curr: Int): Int =
      if (m == 0) prev
      else loop(m - 1, curr, prev + curr)
      
    loop(n, 0, 1)
  }
  
  def isSorted[A](as: Array[A], gt: (A, A) => Boolean): Boolean = {
    @annotation.tailrec
    def _isSorted(prev: A, nextIdx: Int): Boolean = 
      if (nextIdx == as.length) true
      else if (gt(prev, as(nextIdx))) _isSorted(as(nextIdx), nextIdx+1)
      else false
    
    if (as.length == 0) true
    else _isSorted(as(0), 1)
  }

  def partial1[A,B,C](a:A, f: (A,B) => C): B => C = 
    (b: B) => f(a, b)

  def curry[A,B,C](f: (A,B) => C): A => (B => C) = 
    (a: A) => (b: B) => f(a,b)

  def uncurry[A,B,C](f: A => B => C): (A,B) => C =
    (a: A, b: B) => f(a)(b)
    
  def compose[A,B,C](f: B => C, g: A => B): A => C =
    (a: A) => f(g(a))
}