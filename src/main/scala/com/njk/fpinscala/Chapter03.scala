package com.njk.fpinscala

sealed trait List[+A]

case object Nil extends List[Nothing]
case class Cons[+A](head: A, tail: List[A]) extends List[A]

object List {
  def sum(ints: List[Int]): Int = ints match {
    case Nil => 0
    case Cons(x, xs) => x + sum(xs)
  }

  def product(doubles: List[Double]): Double = doubles match {
    case Nil => 0.0
    case Cons(0.0, _) => 0.0
    case Cons(d, ds) => d * product(ds)
  }

  def apply[A](as: A*): List[A] =
    if (as.length == 0) Nil
    else Cons(as.head, apply(as.tail: _*))

  val example = Cons(1, Cons(2, Cons(3, Nil)))
  val example2 = List(1, 2, 3)
  val total = sum(example)

  def append[A](a1: List[A], a2: List[A]): List[A] =
    a1 match {
      case Nil => a2
      case Cons(h, t) => Cons(h, append(t, a2))
    }

  def tail[A](as: List[A]): List[A] =
    as match {
      case Nil => sys.error("Tail of an empty list not possible")
      case Cons(_, t) => t
    }

  def drop[A](as: List[A], n: Int): List[A] =
    if (n <= 0) as
    //else if (n > as.length) Nil
    else as match {
      case Nil => Nil
      case Cons(_, t) => drop(t, n - 1)
    }

  def dropWhile[A](as: List[A])(f: A => Boolean): List[A] = as match {
    //case Nil => Nil
    case Cons(h, t) if (f(h)) => dropWhile(t)(f)
    case _ => as
  }

  def setHead[A](as: List[A], h: A): List[A] = as match {
    case Nil => sys.error("setHead invoked on empty list")
    case Cons(_, t) => Cons(h, t)
  }

  def init[A](as: List[A]): List[A] = as match {
    case Nil => sys.error("init of empty list not possible")
    case Cons(_, Nil) => Nil
    case Cons(h, t) => Cons(h, init(t))
  }

  //This would not work in cases where the accumulator is a collection
  def _foldRight[A, B](as: List[A], z: B)(f: (A, B) => B): B = as match {
    case Nil => z
    case Cons(h, t) => _foldRight(t, f(h, z))(f)
  }

  def foldRight[A, B](as: List[A], z: B)(f: (A, B) => B): B =
    as match {
      case Nil => z
      case Cons(h, t) => f(h, foldRight(t, z)(f))
    }

  def sum2(l: List[Int]) =
    foldRight(l, 0)((x, y) => x + y)

  def product2(l: List[Double]) =
    foldRight(l, 1.0)(_ * _)

  def length[A](as: List[A]): Int =
    foldRight(as, 0)((_, acc) => acc + 1)

  @annotation.tailrec
  def foldLeft[A, B](l: List[A], z: B)(f: (B, A) => B): B = l match {
    case Nil => z
    case Cons(h, t) => foldLeft(t, f(z, h))(f)
  }

  def sum3(as: List[Int]): Int = foldLeft(as, 0)(_ + _)
  def product3(as: List[Double]): Double = foldLeft(as, 1.0)(_ * _)
  
  def reverse[A](as: List[A]): List[A] = foldLeft(as, Nil:List[A])((acc, x) => Cons(x, acc))
  
  def foldRightUsingFoldLeft[A,B](as: List[A], z: B)(f: (A,B) => B): B =
    foldLeft(reverse(as), z)((b, a) => f(a, b))
    
  def appendUsingFoldRight[A](a1: List[A], a2: List[A]): List[A] = 
    foldRight(a1, a2)(Cons(_, _))
    
  /* Exercise 15 - concat() */
  def flatten[A](as: List[List[A]]): List[A] =
    foldRight(as, Nil: List[A])(append)

  def increment(l: List[Int]): List[Int] = 
    foldRight(l, Nil:List[Int]) ((h, t) => Cons(h+1, t))

  def doubleToString(l: List[Double]): List[String] = 
    foldRight(l, Nil:List[String]) ((h, t) => Cons(h.toString, t))
    
  def map[A, B](l: List[A])(f: A => B): List[B] =
    foldRight(l, Nil: List[B])((h, t) => Cons(f(h), t))

  def filter[A](l: List[A])(f: A => Boolean): List[A] =
    foldRight(l, Nil:List[A])((h, t) => if (f(h)) Cons(h, t) else t)
}
